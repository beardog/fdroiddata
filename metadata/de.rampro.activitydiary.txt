Categories:Writing
License:GPL-3.0+
Web Site:https://github.com/ramack/ActivityDiary
Source Code:https://github.com/ramack/ActivityDiary
Issue Tracker:https://github.com/ramack/ActivityDiary/issues

Auto Name:Activity Diary
Summary:Log and plan your recurring activities
Description:
Activity Diary allows to record a diary for your activities. You can attach
notes and pictures to the activies and by that record details like the kind of
vegetables you seeded while gardening. The diary view provides an overview of
what you did and when.

The functions to categorize activites and get a reminder for recurrent actions
are planned.

This App is still under development but already usable. Please contribute to the
[https://github.com/ramack/ActivityDiary github project] and submit bug reports,
feature requests and pull requests!
.

Repo Type:git
Repo:https://github.com/ramack/ActivityDiary

Build:1.0.5,105
    commit=v1.0.5
    subdir=app
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:1.0.5
Current Version Code:105
